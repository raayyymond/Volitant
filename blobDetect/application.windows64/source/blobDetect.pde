import processing.video.*;
import java.awt.Color;
import java.awt.Robot;
import java.awt.event.InputEvent;

Capture cam;

final float lerpFactor = .2;
final int cameraNumber = 5;

ArrayList<Blob> redblobs = new ArrayList<Blob>();
ArrayList<Blob> greenblobs = new ArrayList<Blob>();
ArrayList<Blob> blueblobs = new ArrayList<Blob>();
PVector[] colors = new PVector[6];
int selectColorIndex = 0;
int distthreshold = 10;
int colthreshold = 20;

boolean running = true;
PVector startPos = new PVector();
PVector endPos = new PVector();

PImage frame;

int prevColor;

Robot comp;
int[] lastMouseX = new int[5];
int[] lastMouseY = new int[5];
int lastClick = 0;

void setup() {
  size(1000, 700);

  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(i + ": " + cameras[i]);
    }

    // The camera can be initialized directly using an 
    // element from the array returned by list():
    cam = new Capture(this, cameras[cameraNumber]);
    cam.start();
    frame = new PImage();
  }
  
  try {
    comp = new Robot();
  } catch(Exception e) {
    e.printStackTrace();
  }
}

long prevTime = 0;
long frameTime = 1;
void captureEvent(Capture cam) {
  if (running) {
    cam.read();
    frame = cam.copy();
  }
  frameTime = millis() - prevTime;
  prevTime = millis();
}

void draw() {

  if (running) {
    background(0);
    text(frameRate, width - 200, 50);
    if(frameTime > 0) text("Camera FR: " + 1000/frameTime, width - 200, 30);
    if (mouseX < frame.width && mouseY < frame.height) {
      text(colorVec(frame.pixels[mouseX + mouseY * frame.width]).toString(), width - 200, 70);
    }
    set(0, 0, frame);

    frame.loadPixels();
    loadPixels();

    for (int y = 0; y < frame.height; y++) {
    outer: 
      for (int x = 0; x < frame.width; x++) {

        PVector pixCol = colorVec(hsb(frame.pixels[x + y * frame.width]));
        if (colors[1] != null && pixCol.x >= colors[0].x - colthreshold && pixCol.x <= colors[1].x + colthreshold &&
          pixCol.y >= colors[0].y - colthreshold && pixCol.y <= colors[1].y + colthreshold &&
          pixCol.z >= colors[0].z - colthreshold && pixCol.z <= colors[1].z + colthreshold) {

          pixels[x + y * width] = color(255, 0, 0);
          for (Blob b : redblobs) {
            if (b.isNear(x, y)) {
              b.addPoint(x, y);
              continue outer;
            }
          }
          if (redblobs.size() < 100) redblobs.add(new Blob(x, y, 80));
        }
        if (colors[3] != null && pixCol.x >= colors[2].x - colthreshold && pixCol.x <= colors[3].x + colthreshold &&
          pixCol.y >= colors[2].y - colthreshold && pixCol.y <= colors[3].y + colthreshold &&
          pixCol.z >= colors[2].z - colthreshold && pixCol.z <= colors[3].z + colthreshold) {

          pixels[x + y * width] = color(0, 255, 0);
          for (Blob b : greenblobs) {
            if (b.isNear(x, y)) {
              b.addPoint(x, y);
              continue outer;
            }
          }
          if (redblobs.size() < 100) greenblobs.add(new Blob(x, y, 160));
        }

        if (colors[5] != null && pixCol.x >= colors[4].x - colthreshold && pixCol.x <= colors[5].x + colthreshold &&
          pixCol.y >= colors[4].y - colthreshold && pixCol.y <= colors[5].y + colthreshold &&
          pixCol.z >= colors[4].z - colthreshold && pixCol.z <= colors[5].z + colthreshold) {

          pixels[x + y * width] = color(0, 0, 255);
          for (Blob b : blueblobs) {
            if (b.isNear(x, y)) {
              b.addPoint(x, y);
              continue outer;
            }
          }
          if (redblobs.size() < 100) blueblobs.add(new Blob(x, y, 240));
        }
      }
    }
    updatePixels();

    for (int i = 0; i < redblobs.size() - 1; i++) {
      for (int j = i + 1; j < redblobs.size(); j++) {
        if (redblobs.get(i).isTouching(redblobs.get(j))) {
          redblobs.get(i).combine(redblobs.get(j));
          //redblobs.get(j).c = 0;
          redblobs.get(j).enabled = false;
        }
      }
    }

    for (int i = 0; i < greenblobs.size() - 1; i++) {
      for (int j = i + 1; j < greenblobs.size(); j++) {
        if (greenblobs.get(i).isTouching(greenblobs.get(j))) {
          greenblobs.get(i).combine(greenblobs.get(j));
          //greenblobs.get(j).c = 100;
          greenblobs.get(j).enabled = false;
        }
      }
    }

    for (int i = 0; i < blueblobs.size() - 1; i++) {
      for (int j = i + 1; j < blueblobs.size(); j++) {
        if (blueblobs.get(i).isTouching(blueblobs.get(j))) {
          blueblobs.get(i).combine(blueblobs.get(j));
          //blueblobs.get(j).c = 100;
          blueblobs.get(j).enabled = false;
        }
      }
    }

    for (Blob b : redblobs) {
      b.show();
    }

    for (Blob b : greenblobs) {
      b.show();
    }

    for (Blob b : blueblobs) {
      b.show();
    }

    int bigBlobColor = 0; //0 = none, 1 = red, 2 = green, 3 = blue
  Blob bigBlob = null;
  for (Blob b : redblobs) {
    if (bigBlob == null || bigBlob.getRecSize() < b.getRecSize()) {
      bigBlob = b;
      bigBlobColor = 1;
    }
    b.show();
  }

  for (Blob b : greenblobs) {
    if (bigBlob == null || bigBlob.getRecSize() < b.getRecSize()) {
      bigBlob = b;
      bigBlobColor = 2;
    }
    b.show();
  }

  for (Blob b : blueblobs) {
    if (bigBlob == null || bigBlob.getRecSize() < b.getRecSize()) {
      bigBlob = b;
      bigBlobColor = 3;
    }
    b.show();
  }

  if(bigBlob != null && bigBlob.getRecSize() < 900) {
    bigBlob = null;
    bigBlobColor = 0;
  }

  text(redblobs.size(), width - 200, 90);
  text(greenblobs.size(), width - 200, 110);
  text(blueblobs.size(), width - 200, 130);

  String[] colorText = {"NONE", "RED", "GREEN", "BLUE"};
  text("Color: " + colorText[bigBlobColor], width - 200, 150);

  
  if (bigBlob != null) {
    int minX = 20;
    int maxX = 615;
    int minY = 20;
    int maxY = 450;
    int winWidth = 1920;
    int winHeight = 1080;
    int camWidth = maxX - minX;
    int camHeight = maxY - minY;
    float factorX = winWidth / (float) camWidth;
    float factorY = winHeight / (float) camHeight;
    float x = (bigBlob.maxx + bigBlob.minx) / 2;
    float y = (bigBlob.maxy + bigBlob.miny) / 2;
    int blobX = (int) (winWidth - (x - minX) * factorX);
    int blobY = (int) ((y - minY) * factorY);
    System.out.println(blobX + " : " + blobY);
    
    for(int i = 0; i < lastMouseX.length - 1; i++) {
      lastMouseX[i] = lastMouseX[i + 1];
      lastMouseY[i] = lastMouseY[i + 1];
    }
    lastMouseX[lastMouseX.length - 1] = blobX;
    lastMouseY[lastMouseY.length - 1] = blobY;
    
    float xSum = 0;
    float ySum = 0;
    for(int i = 0; i < lastMouseX.length; i++) {
      xSum += lastMouseX[i];
      ySum += lastMouseY[i];
    }
    xSum /= lastMouseX.length;
    ySum /= lastMouseY.length;
    
    comp.mouseMove((int) xSum, (int) ySum);
    
    if(prevColor == 1 && bigBlobColor == 2) {
      comp.mousePress(InputEvent.BUTTON1_DOWN_MASK);
      lastClick = InputEvent.BUTTON1_DOWN_MASK;
    } else if(prevColor == 2 && bigBlobColor == 1) {
      comp.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
      lastClick = 0;
    } else if(prevColor == 1 && bigBlobColor == 3) {
      comp.mousePress(InputEvent.BUTTON2_DOWN_MASK);
      lastClick = InputEvent.BUTTON2_DOWN_MASK;
    } else if(prevColor == 3 && bigBlobColor == 1) {
      comp.mouseRelease(InputEvent.BUTTON2_DOWN_MASK);
      lastClick = 0;
    } else if(prevColor == 0 && bigBlobColor == 1 && lastClick != 0) {
      comp.mouseRelease(lastClick);
    }
    
  }
    
  prevColor = bigBlobColor;

  redblobs.clear();
  greenblobs.clear();
  blueblobs.clear();
    
  } else {
    background(0);
    set(0, 0, frame);
    stroke(200, 255, 255);
    noFill();
    rectMode(CORNERS);
    rect(startPos.x, startPos.y, endPos.x, endPos.y);
  }
}

void keyPressed() {
  if (key == ' ') {
    running = !running;
    return;
  }
  if (key == 'w') distthreshold++;
  if (key == 's') distthreshold--;
  if (key == 'e') colthreshold++;
  if (key == 'd') colthreshold--;
  println(distthreshold + " d:c " + colthreshold);
}

void mousePressed() {  
  if (!running) {
    startPos.set(mouseX, mouseY);
    endPos.set(mouseX, mouseY);
  }
}

void mouseDragged() {
  if (!running) endPos.set(mouseX, mouseY);
}

void mouseReleased() {
  if (!running) {
    int starty = (int)min(startPos.y, endPos.y);
    int endy = (int)max(startPos.y, endPos.y);
    int startx = (int)min(startPos.x, endPos.x);
    int endx = (int)max(startPos.x, endPos.x);

    frame.loadPixels();

    int rmin, gmin, bmin;
    int rmax, gmax, bmax;
    rmin = gmin = bmin = Integer.MAX_VALUE;
    rmax = gmax = bmax = 0;

    for (int y = starty; y < endy; y++) {
      for (int x = startx; x < endx; x++) {
        PVector c = colorVec(hsb(frame.pixels[x + y * frame.width]));
        rmin = (int) min(rmin, c.x);
        rmax = (int) max(rmax, c.x);
        gmin = (int) min(gmin, c.y);
        gmax = (int) max(gmax, c.y);
        bmin = (int) min(bmin, c.z);
        bmax = (int) max(bmax, c.z);
      }
    }
    colors[selectColorIndex++] = new PVector(rmin, gmin, bmin);
    colors[selectColorIndex++] = new PVector(rmax, gmax, bmax);
    if (selectColorIndex == 6) selectColorIndex = 0;
    println();
    printArray(colors);
  }
}

PVector colorVec(color col) {
  float red = col >> 16 & 0xFF;
  float green = col >> 8 & 0xFF;
  float blue = col & 0xFF;
  return new PVector(red, green, blue);
}

color hsb(color col) {
  float red = col >> 16 & 0xFF;
  float green = col >> 8 & 0xFF;
  float blue = col & 0xFF;
  float[] hsb = Color.RGBtoHSB((int) red, (int) green, (int) blue, null);
  float hue = hsb[0] * 180;
  float sat = hsb[1] * 255;
  float bright = hsb[2] * 255;
  return color(hue, sat, bright);
}

float distSq(float x1, float y1, float x2, float y2) {
  float d = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
  return d;
}
