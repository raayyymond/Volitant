class Blob {

  int minx, miny, maxx, maxy;
  
  boolean enabled = true;
  
  int c = 255;

  Blob(int x, int y, color co) {
    minx = x - 5;
    maxx = x + 5;
    miny = y - 5;
    maxy = y + 5;
    c = co;
  }
  
  boolean isTouching(Blob other){
    if(minx > other.maxx || other.minx > maxx) return false;
    if(miny > other.maxy || other.miny > maxy) return false;
    return true;    
  }
  
  void combine(Blob other){
    minx = min(minx, other.minx);
    miny = min(miny, other.miny);
    maxx = max(maxx, other.maxx);
    maxy = max(maxy, other.maxy);
  }

  boolean isNear(int x, int y) {
    float cx = max(min(x, maxx), minx);
    float cy = max(min(y, maxy), miny);
    float d = distSq(cx, cy, x, y);
    
    return d < distthreshold;
  }

  void addPoint(int x, int y) {
    if (x < minx) minx = (int)lerp(minx, x, lerpFactor);
    else if (x > maxx) maxx = (int)lerp(x, maxx, lerpFactor);

    if (y < miny) miny = (int)lerp(miny, y, lerpFactor);
    else if (y > maxy) maxy = (int)lerp(y, maxy, lerpFactor);
  }

  void show() {
    if (getSize() > 0 && enabled) {
      noFill();
      colorMode(HSB);
      stroke(c, 255, 255);
      strokeWeight(3);
      rectMode(CORNERS);
      rect(minx, miny, maxx, maxy);
      colorMode(RGB);
      
      ellipse((minx + maxx) / 2, (miny + maxy) / 2, 5, 5);
      
    }
  }

  float getSize() {
    return min((maxx - minx), (maxy - miny));
  }
 
  float getRecSize() {
    return abs(maxx - minx) * abs(maxy - miny);
  }
}
