#include <MPU9250.h>

#define RAD_TO_DEGREES 57.2958

unsigned long thyme = 0;
float gx, gy = 0;
int status;
MPU9250 IMU(Wire,0x68);

void setup() {
  Serial.begin(9600);
  while(!Serial) {}

  status = IMU.begin();
  if (status < 0) {
    Serial.println("IMU initialization unsuccessful");
    Serial.println("Check IMU wiring or try cycling power");
    Serial.print("Status: ");
    Serial.println(status);
    while(1) {}
  }
}

void loop() {
  IMU.readSensor();
  gx = gx + IMU.getGyroY_rads() * (millis() - thyme) / 1000;
  gy = gy + IMU.getGyroZ_rads() * (millis() - thyme) / 1000;
  thyme = millis();
  Serial.print(gy*RAD_TO_DEGREES);
  Serial.print(",");
  Serial.println(gx*RAD_TO_DEGREES);
}
